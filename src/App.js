import React from 'react';
import './App.css';
import Boton from '../src/components/Boton';
import { useNavigate } from 'react-router-dom'; // Importa useNavigate
import img1 from '../src/assets/efelante.jpeg';
import img2 from '../src/assets/OIP.jpeg';
import img3 from '../src/assets/pati.jpeg';
import img4 from '../src/assets/orange.jpeg';
import img5 from '../src/assets/R.png';

const App = () => {
	const navigate = useNavigate();

	const handleACClick = () => {
		navigate('/animalesConCors');
	};
	const handleASClick = () => {
		navigate('/animalesSinCors');
	};
	const handleASinClick = () => {
		navigate('/apiSinCors');
	};

	return (
		<div className='App'>
			<header className='App-header'>
				<div style={{ gap: 6, display: 'flex' }}>
					<Boton
						text='Api con Cors'
						color='bg-indigo-500 text-white border-none py-2 px-4 rounded-md shadow-md cursor-pointer'
						click={handleACClick}
					/>
					<Boton
						text='Api con Cors Solucionado '
						color='bg-red-300 text-white border-none py-2 px-4 rounded-md shadow-md cursor-pointer'
						click={handleASClick}
					/>
					<Boton
						text='Api Sin Cors'
						color='bg-slate-600 text-white border-none py-2 px-4 rounded-md shadow-md cursor-pointer'
						click={handleASinClick}
					/>
				</div>
				<div>
					<img src={img1} alt='erre'></img>
					<img src={img2} alt='erre'></img>
					<img src={img3} alt='erre'></img>
					<img src={img4} alt='erre'></img>
					<img src={img5} alt='erre'></img>
				</div>
			</header>
		</div>
	);
};

export default App;
