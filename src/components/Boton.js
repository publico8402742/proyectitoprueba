import React from 'react';
import 'tailwindcss/tailwind.css';

const Button = (props) => {
	return (
		<button className={`${props.color}`} onClick={props.click}>
			{props.text}{' '}
		</button>
	);
};

export default Button;
